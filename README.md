# **bugstream**: *easier analytics event debugging on Android*

## Motivation

Google Analytics events are notoriously to debug: the default `adb logcat` output is dense and tiresome to read, especially when you're already several hours into your workday.

This simple Python script aims to make debugging easier by neatly formatting every analytics related log and dumping them to a handy logfile.    

<br/>

> ⚠️ **Please, be aware this an early prototype**. 
>
>The code is still rather messy. Nonetheless, you can use it without worrying: if the script fails to parse an event log, it will print the original `adb logcat` message.

<br/>

## Requirements

- Python 3.x
- adb

<br/>

## Usage

Simply clone this repository and run the bugstream main script:

```
$ git clone https://gitlab.com/analytics-tools/bugstream.git
$ cd bugstream
$ python3 bugstream.py
```

![Example screenshot](https://i.postimg.cc/R0zTcHGK/bugstream-first-prototype-screenshot-png.png)]