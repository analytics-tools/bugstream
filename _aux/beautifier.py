# --------------------------------------------------
def colored_text(text, color):
    colors = {
        "red": "\033[91m",
        "green": "\033[92m",
        "yellow": "\033[93m",
        "blue": "\033[94m",
        "magenta": "\033[95m",
        "cyan": "\033[96m",
        "white": "\033[97m",
        "grey": "\033[90m",
        "highlighted red": "\033[41m",
        "highlighted green": "\033[42m",
    }
    return colors[color] + text + "\033[0m"


# --------------------------------------------------
def print_upper_border(color=None):
    if color is None:
        print(f"╔{'═' * 68}╗")
    else:
        print(colored_text(f"╔{'═' * 68}╗", color))


# --------------------------------------------------
def print_separator(color=None):
    if color is None:
        print(f"║{'═' * 68}║")
    else:
        print(colored_text(f"║{'═' * 68}║", color))


# --------------------------------------------------
def print_text(text, color=None):
    if color is None:
        print(f"║ {text} {' ' * (66 - len(text))} ║")
    else:
        print(colored_text("║ ", color) + f"{text} {' ' * (66 + 8 - len(text))}" + colored_text(" ║", color))


# --------------------------------------------------
def print_lower_border(color=None):
    if color is None:
        print(f"╚{'═' * 68}╝")
    else:
        print(colored_text(f"╚{'═' * 68}╝", color))


# --------------------------------------------------
def write_upper_border_to_file(logfile):
    logfile.write(f"╔{'═' * 68}╗\n")


# --------------------------------------------------
def write_separator_to_file(logfile):
    logfile.write(f"║{'═' * 68}║\n")


# --------------------------------------------------
def write_text_to_file(text, logfile):
    logfile.write(f"║ {text}{' ' * (66 - len(text))} ║\n")


# --------------------------------------------------
def write_lower_border_to_file(logfile):
    logfile.write(f"╚{'═' * 68}╝\n\n\n")
