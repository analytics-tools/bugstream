#!/usr/bin/env python3
"""
Author : InfiniteFrootLoop <infinitefrootloop@protonmail.com>
Date   : today
Purpose: Firebase (GA4) events debugging on Android
"""

import re
import subprocess
import io
from datetime import datetime
import traceback
from _aux.beautifier import *

# --------------------------------------------------
def enable_adb_logging():
    """Enables adb verbosing mode

    Returns:

    """
    try:
        # Set properties to enable verbose logging for Firebase events
        subprocess.run(["adb", "shell", "setprop", "log.tag.FA", "VERBOSE"])
        subprocess.run(["adb", "shell", "setprop", "log.tag.FA-SVC", "VERBOSE"])

        # Enable debug mode for Nav
        subprocess.run(["adb", "shell", "setprop", "debug.firebase.analytics.app", "br.com.nav"])
        subprocess.run(["adb", "shell", "setprop", "debug.firebase.analytics.app", "br.com.nav.hml"])

        # Clear non-rooted buffers, so that we don't get a barrage of old events logged
        subprocess.run(["adb", "logcat", "-c"])

        # Start logcat process
        logcat_process = subprocess.Popen(["adb", "logcat", "-v", "time", "-s", "FA", "FA-SVC"],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
    except:
        # Print error message here;
        print("Oops, seems like adb isn't properly installed. Exiting...")
        sys.exit(1)
    else:
        return logcat_process


# --------------------------------------------------
def print_event_frame(timestamp, event_name, origin, parameters, border_color):
    print_upper_border(border_color)

    print_text("Timestamp: " + colored_text(timestamp, "yellow"), border_color)

    print_separator(border_color)

    print_text("Event: " + colored_text(event_name, "blue"), border_color)
    print_text("Origin: " + colored_text(origin, "highlighted green" if origin == "app" else "highlighted red"), border_color)

    print_separator(border_color)

    for param in sorted(parameters.keys()):
        print_text(colored_text(f"{param}: {parameters[param]}", "grey" if "(" in param else "cyan"), border_color)

    print_lower_border(border_color)

    print()


# --------------------------------------------------
def write_event_frame_to_file(timestamp, event_name, origin, parameters, logfile):
    write_upper_border_to_file(logfile)

    write_text_to_file(f"Timestamp: {timestamp}", logfile)

    write_separator_to_file(logfile)

    write_text_to_file(f"Event: {event_name}", logfile)
    write_text_to_file(f"Origin: {origin}", logfile)

    write_separator_to_file(logfile)

    for param in sorted(parameters.keys()):
        write_text_to_file(f"{param}: {parameters[param]}", logfile)

    write_lower_border_to_file(logfile)

    logfile.flush()


# --------------------------------------------------
def print_user_prop_frame(timestamp, user_prop, border_color):
        print_upper_border(border_color)

        print_text("Timestamp: " + colored_text(timestamp, "yellow"), border_color)

        print_separator(border_color)

        print_text(colored_text("Setting user property", "green"), border_color)

        print_separator(border_color)

        print_text(colored_text(f"{user_prop.group(1)}: {user_prop.group(2)}", "magenta"), border_color)

        print_lower_border(border_color)

        print()


# --------------------------------------------------
def write_user_prop_frame_to_file(timestamp, user_prop, logfile):
        write_upper_border_to_file(logfile)

        write_text_to_file(f"Timestamp: {timestamp}", logfile)

        write_separator_to_file(logfile)

        write_text_to_file("Setting user property", logfile)

        write_separator_to_file(logfile)

        write_text_to_file(f"{user_prop.group(1)}: {user_prop.group(2)}", logfile)

        write_lower_border_to_file(logfile)

        logfile.flush()


# --------------------------------------------------
def main():
    # Get current date (used later to generate a unique random logfile name)
    current_date = datetime.now()

    # Enable adb logging
    logcat_process = enable_adb_logging()

    # Get logcat output
    logcat_output = io.TextIOWrapper(logcat_process.stdout, encoding="utf-8")

    # Define relevant regular expression patterns
    re_timestamp = re.compile(r"^(\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3})")
    re_event_name = re.compile(r"(name=(.*?))(?:,)")
    re_origin = re.compile(r"(origin=(.*?))(?:,)")
    re_params = re.compile(r"\{([^}]*)\}")
    re_user_prop = re.compile(r"(\S+), (\w+)")

    for line in logcat_output:
        try:
            if "Logging event:" in line:
                # Search for timestamp pattern and return the respective match object
                timestamp = re_timestamp.search(line)

                # Search for event name pattern and return the respective match object
                event_name = re_event_name.search(line)

                # Search for event origin pattern and return the respective match object
                origin = re_origin.search(line)

                # Search for parameter bundle pattern and return the respective match object
                params = re_params.search(line)
                # Extract the parameters inside "Bundle[]"
                params_pairs = params.group(1).split(", ")
                # Split the parameters into key-value pairs
                params_dict = {k: v for k, v in (pair.split("=") for pair in params_pairs)}

                # Print everything into a nice frame
                print_event_frame(timestamp.group(1), event_name.group(2), origin.group(2), params_dict, "blue")

                # Also, dump everything to a logfile
                with open(f"bugstream-{current_date.strftime('%Y-%m-%d-%H-%M-%S')}.log", "a", encoding='utf-8') as logfile:
                    write_event_frame_to_file(timestamp.group(1), event_name.group(2), origin.group(2), params_dict, logfile)

            elif "Setting user property:" in line:
                timestamp = re_timestamp.search(line)
                user_prop = re_user_prop.search(line)

                # Print everything into a nice frame
                print_user_prop_frame(timestamp.group(1), user_prop, "magenta")

                # Also, dump everything to a logfile
                with open(f"bugstream-{current_date.strftime('%Y-%m-%d-%H-%M-%S')}.log", "a", encoding='utf-8') as logfile:
                    write_user_prop_frame_to_file(timestamp.group(1), user_prop, logfile)
        except Exception:
            print(colored_text("ERROR: Couldn't parse log\n", "red"))
            print(colored_text(line, "grey"))
            print(colored_text("EXCEPTION", "red"))
            print("EXCEPTION", "red")
            traceback.print_exc()

# --------------------------------------------------
if __name__ == '__main__':
    main()
